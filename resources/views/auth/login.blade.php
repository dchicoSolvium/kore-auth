<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Panel de administración Kore"/>
    <meta name="author" content="David Chico"/>

    <title>Kore</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <!-- Boostrap   -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <!-- Boostrap select from: https://developer.snapappointments.com/bootstrap-select/ -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script>

</head>
<body class="page-body">


<div class="page-container">
    {{--{#{% if menu is defined %}#}--}}
    {{--{#{% include  '@CQMViewsModule/default/menu.html.twig' %}#}--}}
    {{--{#{% endif %}#}--}}

    <div class="main-content container">

        {{--{#{% include  '@CQMViewsModule/default/header.html.twig' %}#}--}}
        <div class="row">
            <div class="center-block col-sm-6 form-group" style="padding: 50px;">
                <form method="post">
                    <input type="text" class="form-control col-sm-10 center-block" name="username" style="margin-top: 10px; text-align: center;" placeholder="Identificador">
                    <input type="password" class="form-control col-sm-10 center-block" name="password" style="margin-top: 10px; text-align: center;" placeholder="Password">
                    <input type="submit" class="btn btn-primary col-sm-10 center-block" style="margin-top: 10px; text-align: center;" value="Log In">
                </form>
            </div>
        </div>

        <br/>
        <!-- Main Footer -->
        <!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
        <!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
        <!-- Or class "fixed" to  always fix the footer to the end of page -->
        <footer class="main-footer sticky footer-type-1">

            <div class="footer-inner">

                <!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
                <div class="go-up">

                    <a href="#" rel="go-top"> <i class="fa-angle-up"></i>
                    </a>

                </div>

            </div>

        </footer>
    </div>


</div>

</body>
</html>