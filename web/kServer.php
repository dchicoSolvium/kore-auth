<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/04/2019
 * Time: 9:06
 */

include "../vendor/autoload.php";

use kore\Kore;
use kore\KEngine;

Kore::$preprocessor = 'htmlPreprocessor';
Kore::$renderer = 'htmlRenderer';

Kore::addConfig(__DIR__ . '/../chronos');

KEngine::play();

