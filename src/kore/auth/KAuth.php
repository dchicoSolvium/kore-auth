<?php

namespace kore\auth;

use kore\KEngine;
use kore\Kore;
use kore\KPlugin;

/**
 * Created by David Chico.
 * Mail: davidchico@solvium.es
 * Date: 26/03/2019
 */
class KAuth extends KPlugin
{

    public $config = [__DIR__.'/../../../config'];

    public function install()
    {
    }

}