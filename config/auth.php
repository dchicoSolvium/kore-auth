<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */

use kore\Kore;

include_once 'models.php';


// Este componente necesita del uso de variables de session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}


Kore::auth(new \kore\base\Komponent());

Kore::auth()->authModel = 'authUser';


Kore::auth()->loginPage = '/login';
Kore::auth()->redirectPage = '/user/table';
Kore::auth()->redirectModel = 'user';


Kore::auth()->freeActions = [
    'login', 'logout', 'backup', 'restore', 'setup', 'drop', 'reset'
];



Kore::auth()->setUser = function (\kore\base\Entity $user) {
    if ($user->getModel()->getName() == Kore::{Kore::auth()->authModel}()->getName()){
        $_SESSION['KORE_AUTH_USER_ID'] = $user->id;
    }
};


Kore::auth()->getUser = function () {
    if (isset($_SESSION['KORE_AUTH_USER_ID'])){
        return \kore\Kore::getDriver()->getEntity(
            Kore::{Kore::auth()->authModel}(),
            $_SESSION['KORE_AUTH_USER_ID']
        );
    }

    return null;
};


Kore::auth()->isLogged = function () {
    if (isset($_SESSION['KORE_AUTH_USER_ID']) && Kore::auth()->getUser()){
        return true;
    }
};


Kore::auth()->logout = function () {
    unset($_SESSION['KORE_AUTH_USER_ID']);
};

Kore::auth()->goToLogin = function () {
    header("Location: " . Kore::getKore()->baseURL . Kore::auth()->loginPage);
    die();
};

Kore::auth()->goToRedirect = function () {
    header("Location: " . Kore::getKore()->baseURL . Kore::auth()->redirectPage);
    die();
};

Kore::auth()->apiSecurity = function (){
    if (Kore::auth()->apiSecurity && Kore::auth()->apiUser && Kore::$renderer == 'apiRenderer'){
        $apiUser = $_SERVER['kore-api-auth-user'];
        $nonce = $_SERVER['kore-api-auth-nonce'];
        $timestamp = $_SERVER['kore-api-auth-timestamp'];
        $hash = $_SERVER['kore-api-auth-hash'];

        $localHash = hash ( "sha256", $timestamp . $nonce . Kore::auth()->apiKey, true);
        $localHash = base64_encode($localHash);

        if ($apiUser == Kore::auth()->apiUser){
            if (intval($timestamp) - time() < 1000){
                if ($localHash == $hash){
                    return;
                }
            }
        }

        throw new Exception("API AUTH FAILED");
    }

    return;
};


// Verificar permisos
Kore::auth()->preprocess = function (){
    $user = Kore::auth()->getUser();
    $action = Kore::$action;
    $model = Kore::$kmodel;

    if (Kore::$renderer == 'cmdRender' || Kore::$renderer == 'apiRenderer'){
        Kore::auth()->apiSecurity();
        return;
    }


    if (!$user){
        if (!in_array($action, Kore::auth()->freeActions) || !$action){
            Kore::auth()->goToLogin();
        }
        return;
    }

    if (in_array($action, Kore::auth()->freeActions)){
        return;
    }

    \kore\Kore::parentPileServc()->add(Kore::{Kore::auth()->authModel}()->getName(), $user->id);

    if (!Kore::authUser()->userCan($user, $model, $action)  || !$action){
        Kore::auth()->goToRedirect();
        throw new Exception('Usuario sin permisos para acceder aquí');
    }
};


// Redirigir en caso de que haga falta por que se ha logeado el usuario
Kore::auth()->postprocess = function (){
    return;
};