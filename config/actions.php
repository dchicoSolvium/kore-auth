<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 13/05/2019
 * Time: 15:42
 */


use kore\Kore;



Kore::htmlRenderer()->addDir(__DIR__.'/../resources/views');


Kore::login(new \kore\base\KAction([
    'execute' => function() {
        $driver = Kore::getDriver();

        if (isset(Kore::$params['username']) && isset(Kore::$params['password']) ){
            $list = $driver->list(Kore::{Kore::auth()->authModel}(), [
                'username' => Kore::$params['username'],
                'password' => Kore::kPass()->hash(Kore::$params['password'])
            ]);


            if (count($list) > 0){
                Kore::auth()->setUser($list[0]);
                Kore::auth()->goToRedirect();
            }else{
                Kore::allert(new \kore\base\utils\KAllert(
                    "Usuario o contraseña incorrectos",
                    'danger'
                ));
            }
        }

    }
]));

\kore\Kore::htmlRenderer()->login = function (){
    $self = \kore\Kore::kSelf();
    $self->prepareParams();

    $self->render('auth.login');
};


Kore::logout(new \kore\base\KAction([
    'execute' => function() {
        Kore::auth()->logout();
        Kore::auth()->goToLogin();
    }
]));


Kore::changePassword(new \kore\base\KAction([
    'execute' => function() {
        $driver = Kore::getDriver();
        $user = Kore::auth()->getUser();

        if (isset(Kore::$params['newPassword']) && isset(Kore::$params['oldPassword']) && $user){
            $list = $driver->list(Kore::{Kore::auth()->authModel}(), [
                'username' => $user->username,
                'password' => Kore::kPass()->hash(Kore::$params['oldPassword'])
            ]);


            if (count($list) > 0){
                $user->password = Kore::$params['newPassword'];
                Kore::addTarget($user);
                Kore::allert(new \kore\base\utils\KAllert(
                    "Contraseña cambiada correctamente",
                    'success'
                ));
            }else{
                Kore::allert(new \kore\base\utils\KAllert(
                    "Contraseña incorrecta",
                    'danger'
                ));
            }
        }else{
            Kore::allert(new \kore\base\utils\KAllert(
                "Faltan parámetros para cambiar la contraseña",
                'warning'
            ));
        }

    }
]));

\kore\Kore::htmlRenderer()->changePassword = function (){
    if (isset(Kore::$params['reAction'])){
        $kModel = null;

        if (isset(Kore::$params['reModel'])){
            $kModel = Kore::$params['reModel'];
        }

        \kore\KEngine::reAction(Kore::$params['reAction'], $kModel);
    }else{
        \kore\KEngine::reAction('table', Kore::auth()->redirectModel);
    }
};