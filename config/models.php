<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 20/04/2019
 * Time: 17:31
 */

include_once 'types.php';

use kore\Kore;
use kore\base\KModel;
use kore\base\KField;

// Usuarios

Kore::authUser(new KModel([]));


Kore::authUser()->id = new KField([], 'kId');

Kore::authUser()->username = new KField([
    'label' => 'Identificador de usuario'
], 'kString');


Kore::authUser()->password = new KField([
    'label' => 'Contraseña de usuario',
], 'kPass');

Kore::authUser()->role = new KField([
    'label' => 'Rol de usuario',
    'nullable' => true,
    'required' => false,
    'target' => 'authRole',
    'string' => '%name%'
], 'kFK');


Kore::authUser()->title = 'Usuarios autenticables';

Kore::authUser()->actions = ['edit', 'delete'];

Kore::authUser()->tableFields = ['username', 'role'];
Kore::authUser()->editFields = ['username', 'role'];
Kore::authUser()->createFields = ['username', 'role', 'password'];

Kore::authUser()->string = '%username%';

Kore::authUser()->userCan = function (\kore\base\Entity $user, $model, $action='any'){
    $role = Kore::kFK()->getParent($user, 'role');

    $permissions = Kore::getDriver()->list(Kore::authPermision(), [
        'idRole' => $role->id,
    ]);

    $restricction = Kore::getDriver()->list(Kore::authRestriction(), [
        'idRole' => $role->id,
    ]);

    $flag = $role->hasPermision;

    foreach ($permissions as $p){
        if ($p->freeAction != '' && $p->freeAction != null){
            if ($action == $p->freeAction){
                $flag = true;
            }
        }

        if ($p->object == $model){
            if ($p->action == 'all' || $p->action == $action || $action == 'any'){
                $flag = true;
            }
        }
    }

    foreach ($restricction as $p){
        if ($p->freeAction != '' && $p->freeAction != null){
            if ($action == $p->freeAction){
                $flag = false;
            }
        }

        if ($p->object == $model){
            if ($p->action == 'all' || $p->action == $action){
                $flag = false;
            }
        }
    }
    
    return $flag;
};


//// Restricciones

Kore::authRestriction(new KModel([]));

Kore::authRestriction()->id = new KField([], 'kId');

Kore::authRestriction()->freeAction = new KField([
    'label' => 'Acción libre permitida',
], 'kString');

Kore::authRestriction()->object = new KField([
    'label' => 'Modelo sobre el que se aplica la restricción',
], 'kSelect');


Kore::authRestriction()->action = new KField([
    'label' => 'Acción restringida',
    'options' => [
        'create' => 'Create',
        'edit' => 'Edit',
        'delete' => 'Delete',
        'all' => 'All'
    ]
], 'kSelect');

Kore::authRestriction()->idRole = new KField([
    'label' => 'Role',
    'target' => 'authRole',
    'string' => '%name%',
    'required' => true
], 'kFK');



Kore::authRestriction()->title = 'Restricciones';

Kore::authRestriction()->actions = ['edit', 'delete'];

Kore::authRestriction()->tableFields = ['freeAction', 'object', 'action'];
Kore::authRestriction()->editFields = ['freeAction', 'object', 'action'];
Kore::authRestriction()->createFields = ['freeAction', 'object', 'action'];

Kore::authRestriction()->string = '%freeAction% %object% %action% ';

//// Permisos

Kore::authPermision(new KModel([]));

Kore::authPermision()->id = new KField([], 'kId');

Kore::authPermision()->freeAction = new KField([
    'label' => 'Acción libre restringida',
], 'kString');

Kore::authPermision()->object = new KField([
    'label' => 'Modelo sobre el que se aplica el permiso'
], 'kSelect');


Kore::authPermision()->action = new KField([
    'label' => 'Acción permitida',
    'options' => [
        'create' => 'Create',
        'edit' => 'Edit',
        'delete' => 'Delete',
        'all' => 'All'
    ],
], 'kSelect');

Kore::authPermision()->idRole = new KField([
    'label' => 'Role',
    'target' => 'authRole',
    'string' => '%name%',
    'required' => true
], 'kFK');



Kore::authPermision()->title = 'Permisos';

Kore::authPermision()->actions = ['edit', 'delete'];

Kore::authPermision()->tableFields = ['freeAction', 'object', 'action'];
Kore::authPermision()->editFields = ['freeAction', 'object', 'action'];
Kore::authPermision()->createFields = ['freeAction', 'object', 'action'];

Kore::authPermision()->string = '%freeAction% %object% %action% ';


Kore::authPermisionRestrictionPopulate(new \kore\base\KService([
    'execute' => function () {
        $models = Kore::getKore()->findByClass(KModel::class);
        $options = [];

        foreach ($models as $m) {
            $options[$m->getName()] = ($m->title) ? $m->title : $m->getName();
        }

        Kore::authPermision()->object->options = $options;
        Kore::authRestriction()->object->options = $options;
    }
]));



//// Roles

Kore::authRole(new KModel([]));

Kore::authRole()->id = new KField([], 'kId');

Kore::authRole()->name = new KField([
    'label' => 'Nombre',
], 'kString');

Kore::authRole()->description = new KField([
    'label' => 'Descripción',
], 'kString');

Kore::authRole()->hasPermision = new KField([
    'label' => 'Por defecto tiene todos los permisos',
], 'kBool');

Kore::authRole()->krestricctions = new KField([
    'label' => 'Restricciones',
    'target' => 'authRestriction'
], 'kKidFrame');

Kore::authRole()->kpermissions = new KField([
    'label' => 'Permisos',
    'target' => 'authPermision'
], 'kKidFrame');


Kore::authRole()->title = 'Roles de usuario';

Kore::authRole()->actions = ['edit', 'delete'];

Kore::authRole()->tableFields = ['name', 'description', 'hasPermision'];
Kore::authRole()->editFields = ['name', 'description', 'hasPermision', 'kpermissions', 'krestricctions'];
Kore::authRole()->createFields = ['name', 'description', 'hasPermision'];

